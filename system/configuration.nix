{ config, pkgs, ... }:

{
  # Import extensions to configuration.nix (stuff that is specific to my system)
  imports = [ ./hardware-configuration.nix ./tlp.nix ./nvidia.nix ./virt.nix ./systemd.nix];




  # Setting up the hostname (shown in flake.nix)
#  boot.initrd.luks.devices."luks-50e04e3f-6234-4d02-b07e-1a4e87750505".device = "/dev/disk/by-uuid/50e04e3f-6234-4d02-b07e-1a4e87750505";

  boot.initrd.luks.devices."luks-b93ccce6-da64-495f-a627-d2125ae92e41".device = "/dev/disk/by-uuid/b93ccce6-da64-495f-a627-d2125ae92e41";
  networking.hostName = "foxGo";

  # Telling boot to make sure it uses intel module and the rest is just setting up systemd-boot (boot options)
  boot = {
    # Supposed to help speed up firefox
    kernelParams = ["ipv6.disable=1"];
    # Used to add support for NTFS systems at boot
    supportedFilesystems = [ "ntfs" ];

    initrd.kernelModules = [ "amdgpu" ];
    initrd.verbose = false;
    initrd.systemd.enable = true;
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
      efi.efiSysMountPoint = "/boot/efi";
      systemd-boot.configurationLimit = 2;
    };
    # Black listed modules on startup (normally stuff that will just throw a error on boot)
    blacklistedKernelModules = [ "psmouse" ];
    kernelPackages = pkgs.linuxPackages_zen;
  };
  systemd.watchdog.rebootTime = "0";

  # Set your time zone.
  time.timeZone = "Europe/London";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_GB.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_GB.UTF-8";
    LC_IDENTIFICATION = "en_GB.UTF-8";
    LC_MEASUREMENT = "en_GB.UTF-8";
    LC_MONETARY = "en_GB.UTF-8";
    LC_NAME = "en_GB.UTF-8";
    LC_NUMERIC = "en_GB.UTF-8";
    LC_PAPER = "en_GB.UTF-8";
    LC_TELEPHONE = "en_GB.UTF-8";
    LC_TIME = "en_GB.UTF-8";
  };


i18n = {
        inputMethod = {
            enabled = "fcitx5";
            fcitx5.addons = with pkgs; [
                fcitx5-mozc
                fcitx5-gtk
            ];
        };
    };

  # services / packages (for organization)
  services.xserver.enable = false;
  programs.zsh.enable = true;
  security.polkit.enable = true;
  services.gnome.gnome-keyring.enable = true;
  networking.networkmanager.enable = true;
  networking.enableIPv6 = false;

  # Setup user here (make sure you have a password if not defined here)
  users.users.kd = {
    isNormalUser = true;
    description = "Keith H";
    extraGroups = [ "networkmanager" "wheel" "libvirtd" "input"];
    shell = pkgs.zsh;
  };
  # Nix options for running gc automatically (removes the need to run it manually) 
  nix = {
    settings.auto-optimise-store = true;
    settings.allowed-users = [ "kd" ];
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
    };
  };
  # Self explanitory (will also need in home.nix if running standalone)
  nixpkgs.config.allowUnfree = true;

  # Start xdg portal
  xdg = {
    autostart.enable = true;
    portal = {
      enable = true;
      config.common.default = "*";
      extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
    };
  };

  # Setup environment variables (most of this depends on what you wm you are using) echo $environment (variable.name)
  environment.variables = {
    NIXOS_CONFIG = "$HOME/.config/home-manager/system/";
    NIXOS_HOME = "$HOME/.config/home-manager/";
    XDG_DATA_HOME = "$HOME/.local/share";
    GTK_RC_FILES = "$HOME/.local/share/gtk-1.0/gtkrc";
    GTK2_RC_FILES = "$HOME/.local/share/gtk-2.0/gtkrc";
    MOZ_ENABLE_WAYLAND = "1";
    ANKI_WAYLAND = "1";
    DISABLE_QT5_COMPAT = "0";
    NIXOS_OZONE_WL = "0";
    MOZ_WEBRENDER = "1";
  };

  # Stuff installed globally throughout all users (ill take your kneecaps if you dont use nxfmt to format your .nix files)
  environment.systemPackages = with pkgs; [ neovim nixfmt firefox pavucontrol pulseaudio ];


  # Sound setup. Can be changed (use pavucontrol to select devices in use -- even though you arent using pavu to control sound)
  security.rtkit.enable = true;

  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };

  hardware = {
    bluetooth.enable = false;
    opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
    };
  };

  # Setup for flakes and declaration of config.version (dont worry about nix.nixPath)
  nix.settings.experimental-features = [ "nix-command" "flakes" ];
  system.stateVersion = "23.11"; # Did you read the comment?

}

