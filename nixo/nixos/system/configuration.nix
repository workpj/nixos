{ config, pkgs, ... }:

{
 imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./tlp.nix
      ./nvidia.nix
      ./virt.nix
      ./systemd.nix
    ];

 # Bootloader configuration
 boot = {
    kernelParams = [ "ipv6.disable=1" ]; # Disable IPv6
    supportedFilesystems = [ "ntfs" ]; # Support NTFS

    initrd = {
      kernelModules = [ "kvm-amd" ]; # Load AMD modules in initrd
      verbose = false;
      systemd.enable = true;
    };

    loader = {
      systemd-boot.enable = true;
      efi = {
        canTouchEfiVariables = true;
        efiSysMountPoint = "/boot/efi";
      };
      systemd-boot.configurationLimit = 2;
    };

    blacklistedKernelModules = [ "psmouse" ]; # Blacklist psmouse
    kernelPackages = pkgs.linuxPackages_zen; # Use Zen kernel
 };

 # Systemd configuration
 systemd.watchdog.rebootTime = "0";

 # LUKS configuration
 boot.initrd.luks.devices."luks-50e04e3f-6234-4d02-b07e-1a4e87750505".device = "/dev/disk/by-uuid/50e04e3f-6234-4d02-b07e-1a4e87750505";

 # Networking configuration
 networking = {
    hostName = "foxGo"; # Hostname
    networkmanager.enable = true; # Enable NetworkManager
    enableIPv6 = false; # Disable IPv6
 };

 # Time and locale configuration
 time.timeZone = "Europe/London";
 i18n = {
    defaultLocale = "en_GB.UTF-8";
    extraLocaleSettings = {
      LC_ADDRESS = "en_GB.UTF-8";
      LC_IDENTIFICATION = "en_GB.UTF-8";
      LC_MEASUREMENT = "en_GB.UTF-8";
      LC_MONETARY = "en_GB.UTF-8";
      LC_NAME = "en_GB.UTF-8";
      LC_NUMERIC = "en_GB.UTF-8";
      LC_PAPER = "en_GB.UTF-8";
      LC_TELEPHONE = "en_GB.UTF-8";
      LC_TIME = "en_GB.UTF-8";
    };
 };

 # Input method configuration
 i18n.inputMethod = {
    enabled = "fcitx5";
    fcitx5.addons = with pkgs; [
      fcitx5-mozc
      fcitx5-gtk
    ];
 };

 # Console keymap configuration
 console.keyMap = "uk";

 # Services and packages configuration
 services = {
    xserver.enable = false; # Disable X11
    gnome.gnome-keyring.enable = true; # Enable GNOME Keyring
    networkmanager.enable = true; # Enable NetworkManager
 };
 programs.zsh.enable = true; # Enable Zsh
 security.polkit.enable = true; # Enable Polkit

 # User configuration
 users.users.kh = {
    isNormalUser = true;
    description = "Keith H";
    extraGroups = [ "networkmanager" "wheel" "libvirtd" "input" ];
   # shell = pkgs.zsh;
 };

 # Nix configuration
 nix = {
    settings = {
      auto-optimise-store = true;
      allowed-users = [ "kd" ];
    };
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
    };
 };

 # Environment configuration
 environment = {
    systemPackages = with pkgs; [ neovim nixfmt firefox pavucontrol pulseaudio ];
    variables = {
      NIXOS_CONFIG = "$HOME/.config/home-manager/system/";
      NIXOS_HOME = "$HOME/.config/home-manager/";
      XDG_DATA_HOME = "$HOME/.local/share";
      GTK_RC_FILES = "$HOME/.local/share/gtk-1.0/gtkrc";
      GTK2_RC_FILES = "$HOME/.local/share/gtk-2.0/gtkrc";
      MOZ_ENABLE_WAYLAND = "1";
      ANKI_WAYLAND = "1";
      DISABLE_QT5_COMPAT = "0";
      NIXOS_OZONE_WL = "0";
      MOZ_WEBRENDER = "1";
    };
 };

 # Sound configuration
 security.rtkit.enable = true;
 services.pipewire = {
    enable = true;
    alsa = {
      enable = true;
      support32Bit = true;
    };
    pulse.enable = true;
 };

 # Hardware configuration
 hardware = {
    bluetooth.enable = false;
    nvidia.modesetting.enable = true;
    opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
    };
 };

 # Experimental features
 nix.settings.experimental-features = [ "nix-command" "flakes" ];

 # System state version
 system.stateVersion = "23.11";
}

