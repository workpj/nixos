# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix ./tlp.nix ./nvidia.nix ./virt.nix ./systemd.nix
    ];

  # Bootloader.
  #boot.loader.systemd-boot.enable = true;
  #boot.loader.efi.canTouchEfiVariables = true;
# Telling boot to make sure it uses intel module and the rest is just setting up systemd-boot (boot options)
boot = {
    # Supposed to help speed up firefox
    kernelParams = ["ipv6.disable=1"];
    # Used to add support for NTFS systems at boot
    supportedFilesystems = [ "ntfs" ];

    initrd.kernelModules = [ "amd" ];
    initrd.verbose = false;
    initrd.systemd.enable = true;
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
      efi.efiSysMountPoint = "/boot/efi";
      systemd-boot.configurationLimit = 2;
    };
    # Black listed modules on startup (normally stuff that will just throw a error on boot)
    blacklistedKernelModules = [ "psmouse" ];
    kernelPackages = pkgs.linuxPackages_zen;
  };
    systemd.watchdog.rebootTime = "0";



  boot.initrd.luks.devices."luks-50e04e3f-6234-4d02-b07e-1a4e87750505".device = "/dev/disk/by-uuid/50e04e3f-6234-4d02-b07e-1a4e87750505";
  networking.hostName = "foxGo"; # Define your hostname.
  
  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/London";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_GB.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_GB.UTF-8";
    LC_IDENTIFICATION = "en_GB.UTF-8";
    LC_MEASUREMENT = "en_GB.UTF-8";
    LC_MONETARY = "en_GB.UTF-8";
    LC_NAME = "en_GB.UTF-8";
    LC_NUMERIC = "en_GB.UTF-8";
    LC_PAPER = "en_GB.UTF-8";
    LC_TELEPHONE = "en_GB.UTF-8";
    LC_TIME = "en_GB.UTF-8";
  };
  18n = {
        inputMethod = {
            enabled = "fcitx5";
            fcitx5.addons = with pkgs; [
                fcitx5-mozc
                fcitx5-gtk
            ];
        };
    };

  # Configure keymap in X11
  #services.xserver = {
  #  layout = "gb";
  #  xkbVariant = "extd";
  #};

  # Configure console keymap
  console.keyMap = "uk";

  # services / packages (for organization)
  services.xserver.enable = false;
  programs.zsh.enable = true;
  security.polkit.enable = true;
  services.gnome.gnome-keyring.enable = true;
  networking.networkmanager.enable = true;
  networking.enableIPv6 = false;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.kh = {
    isNormalUser = true;
    description = "Keith H";
    extraGroups = [ "networkmanager" "wheel" "libvirtd" "input" ];
    packages = with pkgs; [];
    shell = pkgs.zsh; 
  };

  # Nix options for running gc automatically (removes the need to run it manually) 
  nix = {
    settings.auto-optimise-store = true;
    settings.allowed-users = [ "kd" ];
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
    };
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # Start xdg portal
  xdg = {
    autostart.enable = true;
    portal = {
      enable = true;
      config.common.default = "*";
      extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
    };
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.variables = { 
    NIXOS_CONFIG = "$HOME/.config/home-manager/system/";
    NIXOS_HOME = "$HOME/.config/home-manager/";
    XDG_DATA_HOME = "$HOME/.local/share";
    GTK_RC_FILES = "$HOME/.local/share/gtk-1.0/gtkrc";
    GTK2_RC_FILES = "$HOME/.local/share/gtk-2.0/gtkrc";
    MOZ_ENABLE_WAYLAND = "1";
    ANKI_WAYLAND = "1";
    DISABLE_QT5_COMPAT = "0";
    NIXOS_OZONE_WL = "0";
    MOZ_WEBRENDER = "1";
  };

# Stuff installed globally throughout all users (ill take your kneecaps if you dont use nxfmt to format your .nix files)
  environment.systemPackages = with pkgs; [ neovim nixfmt firefox pavucontrol pulseaudio ];

  # Sound setup. Can be changed (use pavucontrol to select devices in use -- even though you arent using pavu to control sound)
  security.rtkit.enable = true;

  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };

  hardware = {
    bluetooth.enable = false;
    #Most wayland compositors need this
    nvidia.modesetting.enable = true;
    opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
    };
  };
  
  # Enableing hyprland on NixOS
  #programs.hyprland = {
  #  enable = true;
  #  nvidiaPatches = true;
  #  xwayland.enable = true;
  #};

  #environment.sessionVariables = {
  #  # If your cursor becomes invisible
  #  WLR_NO_HARDWARE_CURSORS = "1";
  #  # Hint electron apps to use wauland
  #  NIXOS_OZONE_WL = "1";
  #};

  #hardware = {
  #    # Opengl
  #    opengl.enable = true;
  #    
  #
  #    #Most wayland compositors need this
  #    nvidia.modesetting.enable = true;
  #};

  # Enable flakes
  nix.settings.experimental-features = [ "nix-command" "flakes" ];
 


  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?

}

